package br.com.contatotelefonico.repositories;

import br.com.contatotelefonico.models.Contato;
import org.springframework.data.repository.CrudRepository;

public interface ContatoReporitory extends CrudRepository<Contato, Integer> {
    Iterable<Contato> findAllByIdUsuario(int idUsuario);
}
