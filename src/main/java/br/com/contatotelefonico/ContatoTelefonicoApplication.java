package br.com.contatotelefonico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContatoTelefonicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContatoTelefonicoApplication.class, args);
	}

}
