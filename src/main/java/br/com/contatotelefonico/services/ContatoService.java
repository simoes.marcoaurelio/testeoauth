package br.com.contatotelefonico.services;

import br.com.contatotelefonico.models.Contato;
import br.com.contatotelefonico.repositories.ContatoReporitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

@Service
public class ContatoService {

    @Autowired
    private ContatoReporitory contatoReporitory;

    public Contato gravarContato(Contato contato) {
        return contatoReporitory.save(contato);
    }

    @NewSpan
    public Iterable<Contato> consutarContatosPorUsuario(@SpanTag(key = "idUsuario") int idUsuario) {
        return contatoReporitory.findAllByIdUsuario(idUsuario);
    }
}
