package br.com.contatotelefonico.dtos;

import br.com.contatotelefonico.models.Contato;
import br.com.contatotelefonico.security.Usuario;
import org.springframework.stereotype.Component;

@Component
public class ContatoMapper {

    public Contato paraContato(RequisicaoGravarContato requisicaoGravarContato, Usuario usuario){
        Contato contato = new Contato();

        contato.setIdUsuario(usuario.getId());
        contato.setNomeUsuario(usuario.getName());

        contato.setEmail(requisicaoGravarContato.getEmail());
        contato.setTelefone(requisicaoGravarContato.getTelefone());

        return contato;
    }
}
