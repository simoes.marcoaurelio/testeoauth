package br.com.contatotelefonico.dtos;

public class RequisicaoGravarContato {

    private String email;
    private String telefone;

    public RequisicaoGravarContato() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
