package br.com.contatotelefonico.controllers;

import br.com.contatotelefonico.dtos.ContatoMapper;
import br.com.contatotelefonico.dtos.RequisicaoGravarContato;
import br.com.contatotelefonico.security.Usuario;
import br.com.contatotelefonico.models.Contato;
import br.com.contatotelefonico.services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/contato")

public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @Autowired
    private ContatoMapper contatoMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Contato gravarContato(@RequestBody @Valid RequisicaoGravarContato requisicaoGravarContato,
                                 @AuthenticationPrincipal Usuario usuario) {
        Contato contato = contatoMapper.paraContato(requisicaoGravarContato, usuario);
        return contatoService.gravarContato(contato);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.FOUND)
    public Iterable<Contato> consutarContatosPorUsuario(@AuthenticationPrincipal Usuario usuario) {
        return contatoService.consutarContatosPorUsuario(usuario.getId());
    }
}
